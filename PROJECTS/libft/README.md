# 42/projects/Libft


##### (Francais)

Ce projet est l'un des premiers projets de l'école 42. Il consiste a produire sa propre librairie avec des fonctions utiles au quotidient en C.

Sujet dispo ici :
https://gitlab.com/TheTrueElektro/42/tree/master/SUBJECTS/GENERAL/

###### Attention, cette libft est celle du projet, ma libft à jour est dispo dans mon depo : https://gitlab.com/TheTrueElektro/42/tree/master/FILES/LIBFT/ .




##### (English)

This projects is one of the first projects in 42 school. The goal is to make our own library with the most usefull functions of our C programming life.

Subject available here :
https://gitlab.com/TheTrueElektro/42/tree/master/SUBJECTS/GENERAL/

###### Be carefull, this is the libft project, my actual libft can be found in my repo : https://gitlab.com/TheTrueElektro/42/tree/master/FILES/LIBFT/ .
