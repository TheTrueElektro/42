/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 16:50:45 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/27 15:56:30 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Create and return an element
*/

char		**tab_new(int h, int w)
{
	char	**tab;
	int		i;

	i = -1;
	if (!(tab = (char **)malloc(sizeof(char *) * (h + 1))))
		return (NULL);
	while (++i < h)
		if (!(tab[i] = ft_strcnew(w, '.')))
			return (NULL);
	if (!(tab[i] = ft_strnew(0)))
		return (NULL);
	return (tab);
}

t_tris		*piece_new(char **content, char val)
{
	t_tris		*lst;

	if (!(lst = (t_tris *)malloc(sizeof(t_tris))))
		return (NULL);
	lst->map = content;
	lst->val = val;
	lst->height = ft_tablen(content);
	lst->width = ft_strlen(content[0]);
	lst->next = NULL;
	return (lst);
}

t_cord		*cords_new(int x, int y)
{
	t_cord		*cords;

	if (!(cords = (t_cord *)malloc(sizeof(t_cord))))
		return (NULL);
	cords->x = x;
	cords->y = y;
	return (cords);
}

t_map		*map_new(int sz)
{
	char		**tab;
	t_map		*map;
	int			i;
	int			j;

	i = -1;
	j = -1;
	if (!(tab = (char **)malloc(sizeof(char *) * (sz + 1))))
		return (NULL);
	if (!(map = (t_map *)malloc(sizeof(t_map))))
		return (NULL);
	while (++i < sz)
	{
		if (!(tab[i] = ft_strnew(sz)))
			return (NULL);
		while (++j < sz)
			tab[i][j] = '.';
		j = -1;
	}
	tab[i] = ft_strnew(0);
	map->sz = sz;
	map->tab = tab;
	return (map);
}
