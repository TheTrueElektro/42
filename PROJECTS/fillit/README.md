# 42/projects/fillit


##### (Francais)

Ce projet est l'un des premiers projets de l'école 42. Il consiste a resoudre un puzzle : il s'agit d'agencer des tetriminos (pieces de
4 blocs du celebre jeu tetris) ensemble pour former le plus petit carré possible.

Play with it!
Apres avoir executé la commade de compilation, essayez les patterns mis a disposition dans le dossier patterns !


	make cc
	./gnl patterns/pattern_to_test


Sujet dispo ici :
https://gitlab.com/TheTrueElektro/42/tree/master/SUBJECTS/GENERAL/




##### (English)

This projects is one of the first projects in 42 school. The goal is to solve a puzzle : we have to place the tetriminos (4cases pieces
from the tetris game) together to make the smallest square possible.

Play with it!
After executing the compile command, try the given patterns in the patterns folder !


	make cc
	./gnl patterns/pattern_to_test


Subject available here :
https://gitlab.com/TheTrueElektro/42/tree/master/SUBJECTS/GENERAL/
